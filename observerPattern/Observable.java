package at.kara.observerPattern;

public interface Observable {

	public void inform();
	
	
}
