package at.kara.observerPattern;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Latern l1 = new Latern();
		TrafficLight t1 = new TrafficLight();
		ChristmasTree c1 = new ChristmasTree();
		Sensor s1 = new Sensor();
		
		
		s1.addObservable(c1);
		s1.addObservable(l1);
		s1.addObservable(t1);
		
		s1.all();
	}

}
