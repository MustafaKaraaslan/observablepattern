package at.kara.observerPattern;

import java.util.ArrayList;
import java.util.List;

public class Sensor {
	
	
	private List<Observable> oList;

	public Sensor() {
		super();
		this.oList = new ArrayList<Observable>();
	}
	
	
	public void addObservable(Observable o) {
		
		this.oList.add(o);
		
	}
	
	public void all() {
		
		for(Observable observable : oList) {
			
			observable.inform();
		}
		
	}
	
}
