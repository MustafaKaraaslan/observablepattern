package at.kara.observerPattern;

public interface Component {

	public void isAlive();
	public void start();
}
